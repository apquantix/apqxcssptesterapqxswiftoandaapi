//
//  APQXSwiftOandaAPITester.swift
//  APQXSwiftOandaAPITester
//
//  Created by Bonifatio Hartono on 4/4/16.
//
//

import XCTest
@testable import APQXSwiftOandaAPI
@testable import APQXCSSP

enum XSeqs : String {
    
    /** Construct URL Components */
    case CURLComponents = "Construct URL Components"
    
    /** Construct URLRequest */
    case CURLRequest = "Construct URLRequest"
}
enum XCase : String {
    
    /** Get account */
    case GetAccounts = "Get accounts"
    
    /** Get account information */
    case GetAccountInfo = "Get account information"
    
    /** Get instrument list */
    case GetInstrumentList = "Get instrument list"
    
    /** Get current prices - Polling */
    case GetPricesPoll = "Get current prices - Polling"
    
    /** Get current prices - Streaming */
    case GetPricesStream = "Get current prices - Streaming"
    
    /** Retrieve instrument history */
    case RetrieveInstrumentHistory = "Retrieve instrument history"
    
    /** Get orders */
    case GetOrders = "Get orders"
    
    /** POST orders */
    case PostOrders = "POST orders"
    
    /** Get information for an order */
    case GetOrderInfo = "Get information for an order"
    
    /** Modify an existing order */
    case ModifyOrder = "Modify an existing order"
    
    /** Close an order */
    case CloseOrder = "Close an order"
    
    /** Get list of open trades */
    case GetOpenTrades = "Get list of open trades"
    
    /** Get information on a specific trade */
    case GetTradeInfo = "Get information on a specific trade"
    
    /** Modify an existing trade */
    case ModifyTrade = "Modify an existing trade"
    
    /** Close a trade */
    case CloseTrade = "Close a trade"
    
    /** Get a list of all open positions */
    case GetOpenPositions = "Get a list of all open positions"
    
    /** Get position for an instrument */
    case GetInstrumentPosition = "Get position for an instrument"
    
    /** Close an existing position */
    case ClosePosition = "Close an existing position"
    
    /** Get transaction history */
    case GetTransactionHistory = "Get transaction history"
    
    /** Get information for a transaction */
    case GetTransactionInfo = "Get information for a transaction"
    
    /** Get full account history */
    case GetFullAccountHistory = "Get full account history"
    
    /** Get events (Streaming) */
    case GetEvents = "Get events (Streaming)"
    
    /** Calendar */
    case Calendar = "Calendar"
    
    /** Historical position ratios */
    case HistoricalPositionRatios = "Historical position ratios"
    
    /** Spreads */
    case Spreads = "Spreads"
    
    /** Commitment of Traders */
    case CommitmentOfTraders = "Commitment of Traders"
    
    /** Orderbook */
    case Orderbook = "Orderbook"
    
    /** Autochartist Patterns */
    case Autochartist = "Autochartist Patterns"
}
extension APQX🚧Case{
    convenience init(desc: XCase){
        self.init(desc: desc.rawValue)
    }
}
extension APQX🚧Sequence{
    convenience init(desc: XSeqs){
        self.init(desc: desc.rawValue)
    }
}
class APQXSwiftOandaAPITester: XCTestCase {
    var X : APQX🚧App = APQX🚧App.🔮simpleApp
    
    override class func setUp(){
        super.setUp()
        let X : APQX🚧App = APQX🚧App.🔮simpleApp
        APQX🚧Case(desc: XCase.GetAccounts).load(X)
        APQX🚧Case(desc: XCase.GetAccountInfo).load(X)
        APQX🚧Case(desc: XCase.GetInstrumentList).load(X)
        APQX🚧Case(desc: XCase.GetPricesPoll).load(X)
        APQX🚧Case(desc: XCase.GetPricesStream).load(X)
        APQX🚧Case(desc: XCase.RetrieveInstrumentHistory).load(X)
        APQX🚧Case(desc: XCase.GetOrders).load(X)
        APQX🚧Case(desc: XCase.PostOrders).load(X)
        APQX🚧Case(desc: XCase.GetOrderInfo).load(X)
        APQX🚧Case(desc: XCase.ModifyOrder).load(X)
        APQX🚧Case(desc: XCase.CloseOrder).load(X)
        APQX🚧Case(desc: XCase.GetOpenTrades).load(X)
        APQX🚧Case(desc: XCase.GetTradeInfo).load(X)
        APQX🚧Case(desc: XCase.ModifyTrade).load(X)
        APQX🚧Case(desc: XCase.CloseTrade).load(X)
        APQX🚧Case(desc: XCase.GetOpenPositions).load(X)
        APQX🚧Case(desc: XCase.GetInstrumentPosition).load(X)
        APQX🚧Case(desc: XCase.ClosePosition).load(X)
        APQX🚧Case(desc: XCase.GetTransactionHistory).load(X)
        APQX🚧Case(desc: XCase.GetTransactionInfo).load(X)
        APQX🚧Case(desc: XCase.GetFullAccountHistory).load(X)
        APQX🚧Case(desc: XCase.GetEvents).load(X)
        APQX🚧Case(desc: XCase.Calendar).load(X)
        APQX🚧Case(desc: XCase.HistoricalPositionRatios).load(X)
        APQX🚧Case(desc: XCase.Spreads).load(X)
        APQX🚧Case(desc: XCase.CommitmentOfTraders).load(X)
        APQX🚧Case(desc: XCase.Orderbook).load(X)
        APQX🚧Case(desc: XCase.Autochartist).load(X)
        
        
        APQX🚧Sequence(desc: XSeqs.CURLComponents).load(X)
        APQX🚧Sequence(desc: XSeqs.CURLRequest).load(X)
        
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAPQXSwiftOandaAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    }

    func testGetAccountsCURLComponents(){
        let testResult : NSURLComponents =
            APQX🚧Solution(
                inCase      : XCase.GetAccounts,
                inSeqs  : XSeqs.CURLComponents
                ).formula { (inParam)  in
                    NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Accounts)
                }.load(X, desc: "NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Accounts)").result as! NSURLComponents
        XCTAssertEqual(testResult.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts")
    }
    
    func testGetAccountInfo_CURLComponents(){
        let testResult2 : NSURLComponents =
            APQX🚧Solution(
                inCase      : XCase.GetAccountInfo ,
                inSeqs  : XSeqs.CURLComponents
                ).formula { (inParam)  in
                    NSURLComponents(environment: OandaEnvironment.FXTradePractice, accountID: "12345")
                }.load(X, desc: "NSURLComponents(environment: OandaEnvironment.FXTradePractice, accountID: \"12345\")").result as! NSURLComponents
        XCTAssertEqual(testResult2.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345")
    }
    
    func testGetInstrumentList_CURLComponents(){
        let testResult3 : NSURLComponents =
            APQX🚧Solution(inCase: XCase.GetInstrumentList, inSeqs: XSeqs.CURLComponents).fastFormula {
                NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Instruments, queryItems: [NSURLQueryItem(name: "accountId", value: "12345")])
                }.load(X, desc: "NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Instruments, queryItems: [NSURLQueryItem(name: \"accountId\", value: \"12345\")])").result as! NSURLComponents
        XCTAssertEqual(testResult3.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/instruments?accountId=12345")
    }

    func testGetPricePollCURLComponents(){
        let testResult4 : NSURLComponents =
            APQX🚧Solution().loadCase(XCase.GetPricesPoll).loadSeq(XSeqs.CURLComponents).fastFormula {
                /// TODO: need supporting function to concatenate instruments values.
                NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Prices, queryItems: [NSURLQueryItem(name: "instruments", value: "EUR_USD")])
                }.load(X, desc: "NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Prices, queryItems: [NSURLQueryItem(name: \"instruments\", value: \"EUR_USD\")])").result as! NSURLComponents
        XCTAssertEqual(testResult4.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/prices?instruments=EUR_USD")
    }

    func testGetCurrentPricesStreamingConstructUrlComponents(){
        let result : NSURLComponents! =
            APQX🚧Solution().loadCase(XCase.GetPricesStream).loadSeq(XSeqs.CURLComponents).fastFormula {
                NSURLComponents( environment: OandaEnvironment.FXTradePractice, basePath: OandaStreamingBasePath.Prices, queryItems: [NSURLQueryItem(name: "accountId", value: "12345"), NSURLQueryItem(name: "instruments", value: "EUR_USD")])
                }.load(X, desc:"NSURLComponents( environment: OandaEnvironment.FXTradePractice, basePath: OandaStreamingBasePath.Prices, queryItems: [NSURLQueryItem(name: \"accountId\", value: \"12345\"), NSURLQueryItem(name: \"instruments\", value: \"EUR_USD\")])").result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://stream-fxpractice.oanda.com/v1/prices?accountId=12345&instruments=EUR_USD")
    }

    func testRetrieveInstrumentHistoryConstructUrlComponents(){
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = OandaPollingBasePath.Candles
        let p3 = [NSURLQueryItem(name: "instrument", value: "EUR_USD")]
        let result =
            APQX🚧Solution()
                .loadCase(XCase.RetrieveInstrumentHistory)
                .loadSeq(XSeqs.CURLComponents)
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "OandaPollingBasePath.Candles"))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "[NSURLQueryItem(name: \"instrument\", value: \"EUR_USD\")]"))
                .formula({ (inParam) -> AnyObject in
                    NSURLComponents(environment: p1, basePath: p2, queryItems: p3) })
                .load(X, desc:"NSURLComponents(environment: p1, basePath: p2, queryItems: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/candles?instrument=EUR_USD")
    }

    func testGetOrdersConstructUrlComponents(){
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Orders
        let result =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "'\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Orders"))
                .loadCase(XCase.GetOrders)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc:"NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders")
    }

    func testPostOrdersConstructUrlComponents(){
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Orders
        let result =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "'\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Orders"))
                .loadCase(XCase.PostOrders)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc:"NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders")
    }
    
    func testGetInformationForAnOrderConstructUrlComponents(){
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Orders
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Orders"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.GetOrderInfo)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")
        
    }
    func testModifyAnExistingOrderConstructUrlComponents(){
        
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Orders
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Orders"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.ModifyOrder)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")
        
    }
    
    func testCloseAnOrderConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Orders
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Orders"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.CloseOrder)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")
    }

    func testGetListOfOpenTradesConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Trades
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Trades"))
                .loadCase(XCase.GetOpenTrades)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades")
    }

    func testGetInformationOnASpecificTradeConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Trades
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Trades"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.GetTradeInfo)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
    }

    func testModifyAnExistingTradeConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Trades
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Trades"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.ModifyTrade)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
    }
    
    func testCloseATradeConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Trades
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Trades"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.CloseTrade)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
    }
    
    func testGetAListOfAllOpenPositionsConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Positions
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Positions"))
                .loadCase(XCase.GetOpenPositions)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions")
    }

    func testGetPositionForAnInstrumentConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Positions
        let p4 = "EUR_USD"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Positions"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"EUR_USD\""))
                .loadCase(XCase.GetInstrumentPosition)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions/EUR_USD")
    }

    func testCloseAnExistingPositionConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Positions
        let p4 = "EUR_USD"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Positions"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"EUR_USD\""))
                .loadCase(XCase.ClosePosition)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions/EUR_USD")
    }
    
    func testGetTransactionHistoryConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Transactions
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Transactions"))
                .loadCase(XCase.GetTransactionHistory)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/transactions")
    }
    
    func testGetInformationForATransactionConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.Transactions
        let p4 = "54321"
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.Transactions"))
                .addParam(APQX🚧Parameter(param: p4, paramDesc: "\"54321\""))
                .loadCase(XCase.GetTransactionInfo)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3, detailSubPath: p4)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/transactions/54321")
    }
    
    func testGetFullAccountHistoryConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = "12345"
        let p3 = OandaPollingSubPath.AllTransactions
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "\"12345\""))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "OandaPollingSubPath.AllTransactions"))
                .loadCase(XCase.GetFullAccountHistory)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, accountID: p2, section: p3)
                }
                .load(X, desc: "NSURLComponents(environment: p1, accountID: p2, section: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/alltransactions")
    }
    
    func testGetEventsStreamingConstructUrlComponents () {
        let p1 = OandaEnvironment.FXTradePractice
        let p2 = OandaStreamingBasePath.Prices
        let p3 = [NSURLQueryItem(name: "accountId", value: "12345")]
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(APQX🚧Parameter(param: p1, paramDesc: "OandaEnvironment.FXTradePractice"))
                .addParam(APQX🚧Parameter(param: p2, paramDesc: "OandaStreamingBasePath.Prices"))
                .addParam(APQX🚧Parameter(param: p3, paramDesc: "[NSURLQueryItem(name: \"accountId\", value: \"12345\")]"))
                .loadCase(XCase.GetEvents)
                .loadSeq(XSeqs.CURLComponents)
                .fastFormula {
                    NSURLComponents(environment: p1, basePath: p2, queryItems: p3)
                }
                .load(X, desc: "NSURLComponents(environment: p1, basePath: p2, queryItems: p3)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://stream-fxpractice.oanda.com/v1/prices?accountId=12345")
    }

    
    
    
    
    /* -------------------------- */
    
    override class func tearDown(){
        let X : APQX🚧App = APQX🚧App.🔮simpleApp
        
        
        /** Sequence Integration */
        X.stopAtFirstSignature = true
        X.shouldPrintSignature = true
        X.sequenceIntegrationS()
        
        /* fun generator */
        let aName = "testCalendarConstructUrlComponents"
        let theCase = XCase.Calendar
        let theSeq = XSeqs.CURLComponents
        let anEXPR = "NSURLComponents(environment: p1, basePath: p2, queryItems: p3)"
        let anArray = [
            typeToString(OandaEnvironment.FXTradePractice),
            typeToString(OandaStreamingBasePath.Prices),
            "[NSURLQueryItem(name: \"accountId\", value: \"12345\")]"
        ]
        let anExpectation = "https://stream-fxpractice.oanda.com/v1/prices?accountId=12345"
        let aCase = typeToString(theCase)
        let aSeq = typeToString(theSeq)
        let aType = NSURLComponents.className()
        
        prepareNewTest(aName, inCase: aCase, inSeq: aSeq, expr: anEXPR, type: aType, param: anArray, expected: anExpectation)
        print("\n\n")
    }
}
func typeToString(inAny: Any) -> String{
    var x = String(reflecting: inAny)
    x = x.stringByReplacingOccurrencesOfString("APQXSwiftOandaAPITester.", withString: "")
    x = x.stringByReplacingOccurrencesOfString("APQXSwiftOandaAPI.", withString: "")
    return x
}
extension APQX🚧Solution{
    func loadCase(inCase: XCase) -> APQX🚧Solution{
        self.regCase = inCase.rawValue
        return self
    }
    func loadSeq(inSeq: XSeqs) -> APQX🚧Solution{
        self.regSequence = inSeq.rawValue
        return self
    }
    
    convenience init(inCase: XCase, inSeqs: XSeqs){
        self.init()
        self.loadCase(inCase)
        self.loadSeq(inSeqs)
    }
}
func prepareNewTest(inName: String, inCase: String, inSeq: String, expr: String, type: String, param: NSArray, expected: String) {
    print("\tfunc \(inName) () { ")
    _ = param.enumerate().map { (index, content) in print ("\t\tlet p\(index+1) = \(content)") }
    print ("\t\tlet result : \(type) =")
    print ("\t\t\tAPQX🚧Solution()")
    _ = param.enumerate().map {
        (index, content) in print ("\t\t\t\t.addParam(APQX🚧Parameter(param: p\(index+1), paramDesc: \"\(content)\"))")
    }
    print ("\t\t\t\t.loadCase(\(inCase))")
    print ("\t\t\t\t.loadSeq(\(inSeq))")
    print ("\t\t\t\t.fastFormula {")
    print ("\t\t\t\t\t\(expr)")
    print ("\t\t\t\t}")
    print ("\t\t\t\t.load(X, desc: \"\(expr)\")")
    print ("\t\t\t\t.result as! \(type)")
    print ("\t\tXCTAssertEqual(result.URL?.absoluteString, \"\(expected)\")")
    print ("\t}")
}
