//
//  APQXSwiftOandaAPITester.swift
//  APQXCSSPTesterAPQXSwiftOandaAPI
//
//  Created by Bonifatio Hartono on 4/7/16.
//
//

import XCTest
@testable import APQXSwiftOandaAPI
@testable import APQXCSSP
enum XSeqs : String {
    /** Construct URL Components */
    case CreateURLComponents = "Construct URLComponents"
    /** Construct URLRequest */
    case CreateURLRequest = "Construct URLRequest"
}
enum XCase : String {
    /** Get account */
    case GetAccounts = "GET accounts"
    /** Get account information */
    case GetAccountInfo = "GET account information"
    /** Get instrument list */
    case GetInstrumentList = "GET instrument list"
    /** Get current prices - Polling */
    case GetPricesPoll = "GET current prices Polling"
    /** Get current prices - Streaming */
    case GetPricesStream = "GET current prices Streaming"
    /** Retrieve instrument history */
    case RetrieveInstrumentHistory = "GET instrument history"
    /** Get orders */
    case GetOrders = "GET orders"
    /** POST orders */
    case PostOrders = "POST orders"
    /** Get information for an order */
    case GetOrderInfo = "GET information for an order"
    /** Modify an existing order */
    case ModifyOrder = "PATCH an existing order"
    /** Close an order */
    case CloseOrder = "DELETE an order"
    /** Get list of open trades */
    case GetOpenTrades = "GET list of open trades"
    /** Get information on a specific trade */
    case GetTradeInfo = "GET information on a specific trade"
    /** Modify an existing trade */
    case ModifyTrade = "PATCH an existing trade"
    /** Close a trade */
    case CloseTrade = "DELETE a trade"
    /** Get a list of all open positions */
    case GetOpenPositions = "GET a list of all open positions"
    /** Get position for an instrument */
    case GetInstrumentPosition = "GET position for an instrument"
    /** Close an existing position */
    case ClosePosition = "DELETE an existing position"
    /** Get transaction history */
    case GetTransactionHistory = "GET transaction history"
    /** Get information for a transaction */
    case GetTransactionInfo = "GET information for a transaction"
    /** Get full account history */
    case GetFullAccountHistory = "GET full account history"
    /** Get events (Streaming) */
    case GetEvents = "GET events Streaming"
    /** Calendar */
    case Calendar = "GET Calendar"
    /** Historical position ratios */
    case HistoricalPositionRatios = "GET Historical position ratios"
    /** Spreads */
    case Spreads = "GET Spreads"
    /** Commitment of Traders */
    case CommitmentOfTraders = "GET Commitment of Traders"
    /** Orderbook */
    case Orderbook = "GET Orderbook"
    /** Autochartist Patterns */
    case Autochartist = "GET Autochartist Patterns"
}
extension APQX🚧Case{
    convenience init(desc: XCase){
        self.init(desc: desc.rawValue)
    }
}
extension APQX🚧Sequence{
    convenience init(desc: XSeqs){
        self.init(desc: desc.rawValue)
    }
}

class APQXSwiftOandaAPITester2: XCTestCase {
    var X : APQX🚧App = APQX🚧App.🔮simpleApp
    
    override class func setUp(){
        super.setUp()
        let X : APQX🚧App = APQX🚧App.🔮simpleApp
        APQX🚧Case(desc: XCase.GetAccounts).load(X)
        APQX🚧Case(desc: XCase.GetAccountInfo).load(X)
        APQX🚧Case(desc: XCase.GetInstrumentList).load(X)
        APQX🚧Case(desc: XCase.GetPricesPoll).load(X)
        APQX🚧Case(desc: XCase.GetPricesStream).load(X)
        APQX🚧Case(desc: XCase.RetrieveInstrumentHistory).load(X)
        APQX🚧Case(desc: XCase.GetOrders).load(X)
        APQX🚧Case(desc: XCase.PostOrders).load(X)
        APQX🚧Case(desc: XCase.GetOrderInfo).load(X)
        APQX🚧Case(desc: XCase.ModifyOrder).load(X)
        APQX🚧Case(desc: XCase.CloseOrder).load(X)
        APQX🚧Case(desc: XCase.GetOpenTrades).load(X)
        APQX🚧Case(desc: XCase.GetTradeInfo).load(X)
        APQX🚧Case(desc: XCase.ModifyTrade).load(X)
        APQX🚧Case(desc: XCase.CloseTrade).load(X)
        APQX🚧Case(desc: XCase.GetOpenPositions).load(X)
        APQX🚧Case(desc: XCase.GetInstrumentPosition).load(X)
        APQX🚧Case(desc: XCase.ClosePosition).load(X)
        APQX🚧Case(desc: XCase.GetTransactionHistory).load(X)
        APQX🚧Case(desc: XCase.GetTransactionInfo).load(X)
        APQX🚧Case(desc: XCase.GetFullAccountHistory).load(X)
        APQX🚧Case(desc: XCase.GetEvents).load(X)
        APQX🚧Case(desc: XCase.Calendar).load(X)
        APQX🚧Case(desc: XCase.HistoricalPositionRatios).load(X)
        APQX🚧Case(desc: XCase.Spreads).load(X)
        APQX🚧Case(desc: XCase.CommitmentOfTraders).load(X)
        APQX🚧Case(desc: XCase.Orderbook).load(X)
        APQX🚧Case(desc: XCase.Autochartist).load(X)
        
        
        APQX🚧Sequence(desc: XSeqs.CreateURLComponents).load(X)
        APQX🚧Sequence(desc: XSeqs.CreateURLRequest).load(X)
        
    }
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testGetAccounts(){
        let fCase = XCase.GetAccounts
        // URL Components
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.List)))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.List)))"
        let param1 = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc:"NSURLComponents(inHost: .Address(.HTTPS, .FXTradePractice, .Poll(.Accounts)))")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts")
        
        // URL Request
        let p2 = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
    }
    
    func testGetAccountInformation () {
        let fCase = XCase.GetAccountInfo
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Information(accID: "12345"))))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Information(accID: \"12345\"))))"
        let param1 = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: param1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345")
        
        let p2 = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")

    }
    func testGetInstrumentList () {
        let fCase   = XCase.GetInstrumentList
        let p1      = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Instrument(.DefaultList(accID: "12345"))))
        let p1STR   = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Instrument(.DefaultList(accID: \"12345\"))))"
        let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: p1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/instruments?accountId=12345")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")

    }
    func testGetCurrentPricesPollingConstructUrlcomponents () {
        let fCase = XCase.GetPricesPoll
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Prices))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Prices))"
        let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)

        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: p1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/prices")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")

    }
    func testGetCurrentPricesStreamingConstructUrlcomponents () {
        let fCase = XCase.GetPricesStream
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Stream(.PriceList(accID: "12345", instrument: "EUR_USD", sessionID: "")))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Stream(.PriceList(accID: \"12345\", instrument: \"EUR_USD\", sessionID: \"\")))"
        let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)

        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: p1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://stream-fxpractice.oanda.com/v1/prices?accountId=12345&instruments=EUR_USD&sessionId=")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    
    
    func testGetEventsStreamingConstructUrlcomponents(){
		let fCase = XCase.GetEvents
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Stream(.Events(accountIds: "12345")))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Stream(.Events(accountIds: \"12345\")))"
		let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result : NSURLComponents =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLComponents)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: p1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://stream-fxpractice.oanda.com/v1/events?accountIds=12345")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }

    
    func testGetInstrumentHistoryConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET instrument history")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.DefaultCandles(instrument: "EUR_USD")))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.DefaultCandles(instrument: \"EUR_USD\")))"
		let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result : NSURLComponents =
			APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam(param1)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X, desc: "NSURLComponents(inHost: p1)")
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/candles?instrument=EUR_USD")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetOrdersConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET orders")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.ListDefault(accID: "12345")))))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.ListDefault(accID: \"12345\")))))"
		let param1  = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result : NSURLComponents =
			APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam(param1)
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .load(X,desc: "NSURLComponents(inHost: p1)")
        .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    
    func testPostOrdersConstruct() {
		let fCase = XCase(rawValue: "POST orders")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.ListDefault(accID: "12345")))))
        let p1STR : String  = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.ListDefault(accID: \"12345\")))))"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
			APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders")

        let _ =
            APQX🚧Solution()
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    return "Complex Request"
                }
                .load(X, desc:"Complex Request")
                .result as! String
        
        
        /// Type 1/4 Post Order for Market
        let argp2   = Oanda💡Post.Orders(
            environment: .FXTradePractice,
            accID: "12345",
            instrument: "EUR_USD",
            units: 1,
            side:.Sell,
            type: Oanda💡PostOrderOptionType.Market,
            options: [Oanda💡PostOrderOption.TakeProfit(value: 1.5)])
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: Oanda💡RequestMethod.POST(data: argp2))
        let resultP2 = NSMutableURLRequest(oandaRequest: p2)
        XCTAssertEqual(resultP2.HTTPMethod, "POST")
        XCTAssertEqual(resultP2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(NSString(data: resultP2.HTTPBody!, encoding: NSUTF8StringEncoding), "instrument=EUR_USD&units=1&side=sell&type=market&takeProfit=1.5")
        XCTAssertEqual(resultP2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
        

        /// Type 2/4 Post Order for Limit
        let argp3   = Oanda💡Post.Orders(
            environment: .FXTradePractice,
            accID: "12345",
            instrument: "EUR_USD",
            units: 1,
            side:.Sell,
            type: Oanda💡PostOrderOptionType.Limit(price: 1.5, expiry: NSDate(timeIntervalSinceReferenceDate: 455000000)),
            options: [])
        let p3      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: Oanda💡RequestMethod.POST(data: argp3))
        let resultP3 = NSMutableURLRequest(oandaRequest: p3)
        XCTAssertEqual(resultP3.HTTPMethod, "POST")
        XCTAssertEqual(resultP3.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(NSString(data: resultP3.HTTPBody!, encoding: NSUTF8StringEncoding), "instrument=EUR_USD&units=1&side=sell&type=limit&price=1.5&expiry=2015-06-03T04:53:20Z")
        XCTAssertEqual(resultP3.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetInformationForAnOrderConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET information for an order")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: "12345", orderID: "54321")))))
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: \"12345\", orderID: \"54321\")))))"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    
    func testDeleteAnOrderConstructUrlcomponents () {
		let fCase = XCase(rawValue: "DELETE an order")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: "12345", orderID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: \"12345\", orderID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")

        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .DELETE( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "DELETE")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testPatchAnExistingOrderConstructUrlcomponents () {
		let fCase = XCase(rawValue: "PATCH an existing order")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: "12345", orderID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Order(.Information(accID: \"12345\", orderID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/orders/54321")
        
        let options2 = Oanda💡PatchOrderOption.StopLoss(value: 1.3)
        let m1 = Oanda💡Patch.Orders(environment: .FXTradePractice, accID: "12345", orderID: "54321", options: [options2])
        let p2 = Oanda💡Request.Request(token: .Authorization(token: "example"), method: Oanda💡RequestMethod.PATCH(data: m1))
        let result2 = NSMutableURLRequest(oandaRequest: p2)
        XCTAssertEqual(result2.HTTPMethod, "PATCH")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(NSString(data: result2.HTTPBody!, encoding: NSUTF8StringEncoding), "stopLoss=1.3")
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
        let _ =
        APQX🚧Solution()
            .loadCase(fCase)
            .loadSeq(XSeqs.CreateURLRequest)
            .fastFormula {
                return "Complex Request"
            }
            .load(X, desc:"Complex Request")
            .result as! String


    }
    func testGetListOfOpenTradesConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET list of open trades")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Trade(.List(accID: "12345", options: [])))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, .Poll(.Account(.Trade(.List(accID: \"12345\", options: [])))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetInformationOnASpecificTradeConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET information on a specific trade")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: "12345", tradeID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: \"12345\", tradeID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testPatchAnExistingTradeConstructUrlcomponents () {
		let fCase = XCase(rawValue: "PATCH an existing trade")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: "12345", tradeID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: \"12345\", tradeID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
        

        let p2 = Oanda💡Request.Request(
            token: .Authorization(token: "example"),
            method: .PATCH(
                data: .Trades(
                    environment: .FXTradePractice,
                    accID: "12345",
                    tradeID: "54321",
                    options: [Oanda💡PatchTradeOption.StopLoss(value: 1.3)]
                )
            )
        )
        let p2STR = "Oanda💡Request.Request.Request(token: .Authorization(token: \"example\"), method: .PATCH(data: .Trades(environment: .FXTradePractice, accID: \"12345\", tradeID: \"54321\", options: [Oanda💡PatchTradeOption.StopLoss(value: 1.3)])))"
		let param2  = APQX🚧Parameter(param: p2, paramDesc: p2STR)
        let result2 =
            APQX🚧Solution()
                .loadCase(fCase)
                .loadSeq(XSeqs(rawValue: "Construct URLRequest")!)
                .addParam( param2 )
                
                .load(X,desc: "NSMutableURLRequest(oandaRequest: p2)")
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .result as! NSMutableURLRequest
        XCTAssertEqual(result2.HTTPMethod, "PATCH")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(NSString(data: result2.HTTPBody!, encoding: NSUTF8StringEncoding), "stopLoss=1.3")
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")

        
    }
    func testDeleteATradeConstructUrlcomponents () {
		let fCase = XCase(rawValue: "DELETE a trade")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: "12345", tradeID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: \"12345\", tradeID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/trades/54321")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .DELETE( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "DELETE")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")

    }
    func testGetAListOfAllOpenPositionsConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET a list of all open positions")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.List(accID: "12345")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.List(accID: \"12345\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetPositionForAnInstrumentConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET position for an instrument")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.Information(accID: "12345", instrumentID: "EUR_USD")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.Information(accID: \"12345\", instrumentID: \"EUR_USD\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions/EUR_USD")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testDeleteAnExistingPositionConstructUrlcomponents () {
		let fCase = XCase(rawValue: "DELETE an existing position")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.Information(accID: "12345", instrumentID: "EUR_USD")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Position(.Information(accID: \"12345\", instrumentID: \"EUR_USD\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/positions/EUR_USD")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .DELETE( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "DELETE")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    
    func testGetTransactionHistoryConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET transaction history")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.History(accID: "12345", options: [])))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.History(accID: \"12345\", options: [])))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/transactions")

        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetInformationForATransactionConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET information for a transaction")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.Information(accID: "12345", transactionID: "54321")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.Information(accID: \"12345\", transactionID: \"54321\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/transactions/54321")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetFullAccountHistoryConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET full account history")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.AllTransaction(accID: "12345")))) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Account(.Transaction(.AllTransaction(accID: \"12345\")))) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts/12345/alltransactions")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetCalendarConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Calendar")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Calendar(period: .OneDay, option: [])) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Calendar(period: .OneDay, option: [])) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/calendar?period=86400")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }

    func testGetHistoricalPositionRatiosConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Historical position ratios")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.HistoricalPositionRatios(.EUR_USD, .OneWeek)) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.HistoricalPositionRatios(.EUR_USD, .OneWeek)) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/historical_position_ratios?instrument=EUR_USD&period=604800")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetSpreadsConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Spreads")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Spreads(instrument: "EUR_USD", period: Oanda💡SpreadOptionPeriod.OneDay)) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.Spreads(instrument: \"EUR_USD\", period: Oanda💡SpreadOptionPeriod.OneDay)) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/spreads?instrument=EUR_USD&period=86400")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetCommitmentOfTradersConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Commitment of Traders")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.CommitmentsOfTraders(instrument: .EUR_USD)) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.CommitmentsOfTraders(instrument: .EUR_USD)) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/commitments_of_traders?instrument=EUR_USD")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetOrderbookConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Orderbook")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.OrderBook(instrument: .EUR_USD, period: .OneDay)) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.OrderBook(instrument: .EUR_USD, period: .OneDay)) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/orderbook_data?instrument=EUR_USD&period=86400")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }
    func testGetAutochartistPatternsConstructUrlcomponents () {
		let fCase = XCase(rawValue: "GET Autochartist Patterns")!
        let p1 = Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.AutoChartList(option: [])) )
        let p1STR = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, Oanda💡Type.Poll(.AutoChartList(option: [])) )"
        let param1 : APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)
        let result =
            APQX🚧Solution()
				.loadCase(fCase)
				.loadSeq(XSeqs(rawValue: "Construct URLComponents")!)
                .addParam( param1 )
                
                .load(X,desc: "NSURLComponents(inHost: p1)")
                .fastFormula {
                    NSURLComponents(inHost: p1)
                }
                .result as! NSURLComponents
        XCTAssertEqual(result.URL?.absoluteString, "https://api-fxpractice.oanda.com/labs/v1/signal/autochartist")
        
        let p2      = Oanda💡Request.Request( token: .Authorization(token: "example"), method: .GET( address: p1 ) )
        let result2 =
            APQX🚧Solution()
                .addParam(param1)
                .loadCase(fCase)
                .loadSeq(XSeqs.CreateURLRequest)
                .fastFormula {
                    NSMutableURLRequest(oandaRequest: p2)
                }
                .load(X, desc:"NSMutableURLRequest(oandaRequest: p2)")
                .result as! NSURLRequest
        XCTAssertEqual(result2.HTTPMethod, "GET")
        XCTAssertEqual(result2.URL?.absoluteString, result.URL?.absoluteString)
        XCTAssertEqual(result2.HTTPBody, nil)
        XCTAssertEqual(result2.valueForHTTPHeaderField("Authorization"), "Bearer example")
        
    }

    /* TODO: Insert Above Here */
    
    
    static var theOandaType : String = ""
    static var theTestResult : String = ""
    
    /* TODO: Test Function below here */
    
    func testRequestGenerator(){
        let aComponents = NSURLComponents(inHost: .Address(.HTTPS, .FXTradePractice, .Poll(.Account(.List))))
        print(aComponents)
    }
    
    func testGenerator(){

        // User Edit
        let anOandaType = Oanda💡Type.Poll(.AutoChartList(option: []))
        let testResult = "https://api-fxpractice.oanda.com/labs/v1/signal/autochartist"
        // Don't Forget this one
        APQXSwiftOandaAPITester2.theOandaType = "Oanda💡Type.Poll(.AutoChartList(option: []))"
        
        
        // Auto Gen
        
        APQXSwiftOandaAPITester2.theTestResult = testResult
        let xresult = NSURLComponents(inHost:
            Oanda💡Host.Address(.HTTPS, .FXTradePractice, anOandaType)
        )
        XCTAssertEqual(xresult.URL?.absoluteString, testResult)
    }
    
    override class func tearDown(){
        let X : APQX🚧App = APQX🚧App.🔮simpleApp
        
        
        /** Sequence Integration */
        X.compileSequences()
        X.stopAtFirstSignature = true
        X.shouldPrintSignature = true
        X.sequenceIntegrationS()
        
        
        print("\n\n")
        
        /* another fun generator */
        //functionGenerator()
        //functionGenerator()
        /*-----------------------*/
        
        
        // print("test\(X.casseqsDesc[inString]!.functionalizedString())")
    }

}
func functionGenerator(){
    
    // Config
    let anOandaTypeSTR : String = APQXSwiftOandaAPITester2.theOandaType
    let testResult : String = APQXSwiftOandaAPITester2.theTestResult
    // Config
    
    let param = "Oanda💡Host.Address(.HTTPS, .FXTradePractice, \(anOandaTypeSTR) )"
    let paramOanda = "APQX🚧Parameter = APQX🚧Parameter(param: p1, paramDesc: p1STR)"
    let expr  = "NSURLComponents(inHost: p1)"

    let X : APQX🚧App = APQX🚧App.🔮simpleApp
    var stop = false
    _ = X.casseqsIndexS.map{
        if stop {
            return
        }
        if X.casseqs[$0] != nil {
            // print(X.casseqs[$0]?.regCase)
            return
        }
        let funcTitle = X.casseqsDesc[$0]!.functionalizedString()
        
        print("✍️✍️✍️")
        print("func test\(X.casseqsDesc[$0]!.functionalizedString()) () {")
        _ = X.casseqsDesc.map{(inKey, inValue) in
            if funcTitle == inValue.functionalizedString() {
                stop = true
                let newVal = inValue.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: "@"))
                print ("let p1 = \(param)")
                let backslashedParam = param.stringByReplacingOccurrencesOfString("\"", withString: "\\\"")
                print ("let p1STR = \"\(backslashedParam)\"") // should add backslash before "
                print ("let paramOanda : \(paramOanda)")
                print ("let result = ")
                print ("\tAPQX🚧Solution(inCase: \"\(newVal[0])\", inSeqs: \"\(newVal[1])\")")
                print (".addParam( paramOanda )")
                print ("")
                print ("\t.load(X,desc: \"\(expr)\")")
                print ("\t.fastFormula {")
                print ("\t\(expr)")
                print ("\t}")
                print ("\t.result as! NSURLComponents")
                print ("\tXCTAssertEqual(result.URL?.absoluteString, \"\(testResult)\")")
            }
        }
        print("}")
    }
}


func prepareNewTest(inName: String, inCase: String, inSeq: String, expr: String, type: String, param: NSArray, expected: String) {
    
    print("\tfunc \(inName) () { ")
    _ = param.enumerate().map { (index, content) in print ("\t\tlet p\(index+1) = \(content)") }
    print ("\t\tlet result : \(type) =")
    print ("\t\t\tAPQX🚧Solution()")
    _ = param.enumerate().map {
        (index, content) in print ("\t\t\t\t.addParam(APQX🚧Parameter(param: p\(index+1), paramDesc: \"\(content)\"))")
    }
    print ("\t\t\t\t.loadCase(\(inCase))")
    print ("\t\t\t\t.loadSeq(\(inSeq))")
    print ("\t\t\t\t.fastFormula {")
    print ("\t\t\t\t\t\(expr)")
    print ("\t\t\t\t}")
    print ("\t\t\t\t.load(X, desc: \"\(expr)\")")
    print ("\t\t\t\t.result as! \(type)")
    print ("\t\tXCTAssertEqual(result.URL?.absoluteString, \"\(expected)\")")
    print ("\t}")
}

func typeToString(inAny: Any) -> String{
    var x = String(reflecting: inAny)
    x = x.stringByReplacingOccurrencesOfString("APQXSwiftOandaAPITester.", withString: "")
    x = x.stringByReplacingOccurrencesOfString("APQXSwiftOandaAPI.", withString: "")
    return x
}

extension APQX🚧Solution{
    func loadCase(inCase: XCase) -> APQX🚧Solution{
        self.regCase = inCase.rawValue
        return self
    }
    func loadSeq(inSeq: XSeqs) -> APQX🚧Solution{
        self.regSequence = inSeq.rawValue
        return self
    }
}